package org.buffalocoder.quanlyquancafe.server.helpers;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class JpaPersistenceHelper {
    private static JpaPersistenceHelper _instance;
    private static EntityManager entityManager;
    private static EntityTransaction entityTransaction;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public EntityTransaction getEntityTransaction() {
        return entityTransaction;
    }

    private JpaPersistenceHelper() {
        entityManager = Persistence.createEntityManagerFactory("QuanLyQuanCafe").createEntityManager();
        entityTransaction = entityManager.getTransaction();
    }

    public static JpaPersistenceHelper getInstance() {
        if (_instance == null) {
            synchronized (JpaPersistenceHelper.class) {
                if (null == _instance) {
                    _instance = new JpaPersistenceHelper();
                }
            }
        }

        return _instance;
    }
}
