package org.buffalocoder.quanlyquancafe.server;

import org.buffalocoder.quanlyquancafe.common.entities.*;
import org.buffalocoder.quanlyquancafe.common.exceptions.ValidateException;
import org.buffalocoder.quanlyquancafe.common.types.GioiTinhEnum;
import org.buffalocoder.quanlyquancafe.common.types.LoaiDienThoaiEnum;
import org.buffalocoder.quanlyquancafe.common.types.LoaiNhanVienEnum;
import org.buffalocoder.quanlyquancafe.server.daos.*;
import org.buffalocoder.quanlyquancafe.common.interfaces.*;
import org.buffalocoder.quanlyquancafe.server.helpers.JpaPersistenceHelper;
import org.buffalocoder.quanlyquancafe.server.helpers.RegisterRMIHelper;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Nguyễn Chí Cường
 * @since 05-10-2019
 * @version 1.0
 */
public class MainServer {
    private static final int PORT = 9999;
    private static InetAddress inetAddress;

    public static void main(String[] args) throws RemoteException, AlreadyBoundException, ValidateException {
        System.out.println("JPA initializing ...");
        JpaPersistenceHelper.getInstance();
        System.out.println("JPA initialized !!!");

        System.out.println("\n---------- RMI Register Object ----------");

        try {
            inetAddress = InetAddress.getLocalHost();
            System.setProperty("java.rmi.server.hostname", "172.16.0.117");
        } catch (UnknownHostException e) {
            System.err.println("Can't get information host");
        }


        RegisterRMIHelper registerRMIHelper = RegisterRMIHelper.getInstance(PORT);
        registerRMIHelper.registerObject(IMonAnDAO.class.getSimpleName(), new MonAnDAOImpl());
        registerRMIHelper.registerObject(IBanDAO.class.getSimpleName(), new BanDAOImpl());
        registerRMIHelper.registerObject(IDanhMucMonAnDAO.class.getSimpleName(), new DanhMucMonAnDAOImpl());
        registerRMIHelper.registerObject(IHoaDonDAO.class.getSimpleName(), new HoaDonDAOImpl());
        registerRMIHelper.registerObject(INhanVienDAO.class.getSimpleName(), new NhanVienDAOImpl());

        System.out.println("\n---------- SERVER INFORMATION ----------");
        System.out.println(String.format("[HOSTNAME]: %s", inetAddress.getHostName()));
        System.out.println(String.format("[PORT]: %s", PORT));

        System.out.println("\nServer started !!!");
        initData();
    }

    public static void initData() throws ValidateException, RemoteException {
        INhanVienDAO nhanVienDAO = new NhanVienDAOImpl();

        NhanVien nhanVien = new NhanVien(
                nhanVienDAO.generateId(),
                "Admin",
                "12345678",
                GioiTinhEnum.KHONG_XAC_DINH,
                LocalDate.of(1999, 1, 1),
                "cafe999@gmail.com",
                LocalDate.now(),
                LoaiNhanVienEnum.ADMIN,
                "345626115",
                new DiaChi(
                        "12 Nguyen Van Bao",
                        "Phuong 4",
                        "Quan Go Vap",
                        "Thanh pho Ho Chi Minh"
                ),
                new DienThoai(
                        LoaiDienThoaiEnum.DI_DONG,
                        "0945836291"
                )
        );
        nhanVienDAO.add(nhanVien);
    }
}
