package org.buffalocoder.quanlyquancafe.server.daos;

import org.buffalocoder.quanlyquancafe.common.consts.CollectionNameConst;
import org.buffalocoder.quanlyquancafe.common.consts.PatternIdConst;
import org.buffalocoder.quanlyquancafe.common.entities.MonAn;
import org.buffalocoder.quanlyquancafe.common.entities.NhanVien;
import org.buffalocoder.quanlyquancafe.common.exceptions.NotExistException;
import org.buffalocoder.quanlyquancafe.common.exceptions.ValidateException;
import org.buffalocoder.quanlyquancafe.common.interfaces.INhanVienDAO;
import org.mindrot.jbcrypt.BCrypt;

import javax.persistence.EntityManager;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public class NhanVienDAOImpl extends DataAccessObjectImpl<NhanVien> implements INhanVienDAO {
	public NhanVienDAOImpl(EntityManager entityManager) throws RemoteException {
        super(entityManager, NhanVien.class);
        createIndex();
    }

    public NhanVienDAOImpl() throws RemoteException {
        super(NhanVien.class);
        createIndex();
    }

    @Override
    public List<NhanVien> find() {
        List<NhanVien> nhanVienList = new ArrayList<>();
        String query = String.format("db.%s.find({})", CollectionNameConst.EMPLOYEE_COLLECTION);

        try {
            List<?> list = this.entityManager.createNativeQuery(query, NhanVien.class).getResultList();
            list.forEach(item -> nhanVienList.add((NhanVien) item));
        } catch (Exception e) {
            return null;
        }

        return nhanVienList;
    }

    @Override
    public NhanVien dangNhap(String maNhanVien, String matKhau) throws Exception {
	    // TODO tai khoan test
        if (maNhanVien.equals("admin") && matKhau.equals("123")) {
            return new NhanVien();
        }

	    try {
	        NhanVien nhanVien = this.find(maNhanVien);

	        if (nhanVien != null) {
	            boolean isCorrectPassword = nhanVien.checkPassword(matKhau);

	            if (isCorrectPassword) {
	                return nhanVien;
                } else throw new Exception("Sai mật khẩu");
            } else throw new NotExistException("Không tìm thấy nhân viên");
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public boolean capNhatMatKhau(String maNhanVien, String matKhauCu, String matKhauMoi) throws Exception {
	    NhanVien nhanVien = find(maNhanVien);
	    int ketQua = 0;

	    if (nhanVien == null) {
	        throw new NotExistException("Nhân viên không tồn tại");
        }

	    if (nhanVien.checkPassword(matKhauCu)) {
            String query = String.format("db.%s.updateOne({_id: '%s'}, {$set: {matKhau: '%s'}})",
                    CollectionNameConst.EMPLOYEE_COLLECTION, maNhanVien, BCrypt.hashpw(matKhauMoi, BCrypt.gensalt(12)));

            System.out.println(query);

            try {
                this.entityTransaction.begin();
                ketQua = this.entityManager.createNativeQuery(query).executeUpdate();
                this.entityTransaction.commit();
            } catch (Exception e) {
                this.entityTransaction.rollback();
                return false;
            }
        } else throw new ValidateException("Mật khẩu hiện tại không đúng");

        return ketQua != 0;
    }

    @Override
    public void createIndex() throws RemoteException {
        String query = String.format("db.%s.createIndex({'$**': 'text'})", CollectionNameConst.EMPLOYEE_COLLECTION);

        try {
            this.entityTransaction.begin();
            this.entityManager.createNativeQuery(query);
            this.entityTransaction.commit();
        } catch (Exception e) {
            this.entityTransaction.rollback();
        }
    }

    @Override
    public List<NhanVien> fullTextSearch(String keyword) throws RemoteException {
        createIndex();
        List<NhanVien> nhanViens = new ArrayList<>();
        String query = String.format("db.%s.find({$text: {$search: '%s'}})",
                CollectionNameConst.EMPLOYEE_COLLECTION, keyword);
        try {
            List<?> list = this.entityManager.createNativeQuery(query, NhanVien.class).getResultList();
            list.forEach(item -> nhanViens.add((NhanVien) item));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return nhanViens;
    }

    @Override
    public String generateId() throws RemoteException {
        int lastId = 0;
        String query = String.format("db.%s.find({}).limit(1).sort({'_id': -1})", CollectionNameConst.EMPLOYEE_COLLECTION);

        try {
            List<?> list = this.entityManager.createNativeQuery(query, NhanVien.class).getResultList();
            NhanVien nhanVienLast = (NhanVien) list.get(list.size() - 1);

            lastId = Integer.parseInt(nhanVienLast.getMaNhanVien()
                    .substring(PatternIdConst.PREFIX_NHAN_VIEN.length(), nhanVienLast.getMaNhanVien().length()));
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }

        return String.format("%s%04d", PatternIdConst.PREFIX_NHAN_VIEN, ++lastId);
    }
}
