package org.buffalocoder.quanlyquancafe.server.daos;

import org.buffalocoder.quanlyquancafe.common.consts.CollectionNameConst;
import org.buffalocoder.quanlyquancafe.common.consts.PatternIdConst;
import org.buffalocoder.quanlyquancafe.common.entities.HoaDon;
import org.buffalocoder.quanlyquancafe.common.entities.MonAn;
import org.buffalocoder.quanlyquancafe.common.interfaces.IMonAnDAO;

import javax.persistence.EntityManager;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public class MonAnDAOImpl extends DataAccessObjectImpl<MonAn> implements IMonAnDAO {
	public MonAnDAOImpl(EntityManager entityManager) throws RemoteException {
        super(entityManager, MonAn.class);
        createIndex();
    }

    public MonAnDAOImpl() throws RemoteException {
        super(MonAn.class);
        createIndex();
    }

    @Override
    public List<MonAn> find() {
        List<MonAn> monAnList = new ArrayList<>();
        String query = String.format("db.%s.find({})", CollectionNameConst.FOOD_COLLECTION);
        try {
            List<?> list = this.entityManager.createNativeQuery(query, MonAn.class).getResultList();
            list.forEach(item -> monAnList.add((MonAn) item));
        } catch (Exception e) {
            return null;
        }

        return monAnList;
    }

    @Override
    public void createIndex() throws RemoteException {
        String query = String.format("db.%s.createIndex({'$**': 'text'})", CollectionNameConst.FOOD_COLLECTION);

        try {
            this.entityTransaction.begin();
            this.entityManager.createNativeQuery(query);
            this.entityTransaction.commit();
        } catch (Exception e) {
            this.entityTransaction.rollback();
        }
    }

    @Override
    public List<MonAn> fullTextSearch(String keyword) throws RemoteException {
        createIndex();
        List<MonAn> monAns = new ArrayList<>();
        String query = String.format("db.%s.find({$text: {$search: '%s'}})",
                CollectionNameConst.FOOD_COLLECTION, keyword);
        try {
            List<?> list = this.entityManager.createNativeQuery(query, MonAn.class).getResultList();
            list.forEach(item -> monAns.add((MonAn) item));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return monAns;
    }

    @Override
    public List<MonAn> findByMaDanhMuc(String maDanhMuc) {
        List<MonAn> monAns = new ArrayList<>();
        String query = String.format("db.%s.find({maDanhMuc: '%s'})", CollectionNameConst.FOOD_COLLECTION, maDanhMuc);

        try {
            List<?> list = this.entityManager.createNativeQuery(query, MonAn.class).getResultList();
            list.forEach(item -> monAns.add((MonAn) item));
        } catch (Exception e) {
            return null;
        }

        return monAns;
    }

    @Override
    public String generateId() throws RemoteException {
        int lastId = 0;
        String query = String.format("db.%s.find({}).limit(1).sort({'_id': -1})", CollectionNameConst.FOOD_COLLECTION);

        try {
            List<?> list = this.entityManager.createNativeQuery(query, MonAn.class).getResultList();
            MonAn monAnLast = (MonAn) list.get(list.size() - 1);

            lastId = Integer.parseInt(monAnLast.getMaMonAn()
                    .substring(PatternIdConst.PREFIX_MON_AN.length(), monAnLast.getMaMonAn().length()));
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }

        return String.format("%s%04d", PatternIdConst.PREFIX_MON_AN, ++lastId);
    }
}
