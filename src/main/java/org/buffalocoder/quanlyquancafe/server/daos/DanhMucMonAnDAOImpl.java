package org.buffalocoder.quanlyquancafe.server.daos;

import org.buffalocoder.quanlyquancafe.common.consts.CollectionNameConst;
import org.buffalocoder.quanlyquancafe.common.consts.PatternIdConst;
import org.buffalocoder.quanlyquancafe.common.entities.Ban;
import org.buffalocoder.quanlyquancafe.common.entities.DanhMucMonAn;
import org.buffalocoder.quanlyquancafe.common.interfaces.IDanhMucMonAnDAO;

import javax.persistence.EntityManager;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public class DanhMucMonAnDAOImpl extends DataAccessObjectImpl<DanhMucMonAn> implements IDanhMucMonAnDAO {
	public DanhMucMonAnDAOImpl(EntityManager entityManager) throws RemoteException {
        super(entityManager, DanhMucMonAn.class);
        createIndex();
    }

    public DanhMucMonAnDAOImpl() throws RemoteException {
        super(DanhMucMonAn.class);
        createIndex();
    }

    @Override
    public List<DanhMucMonAn> find() {
        List<DanhMucMonAn> danhMucMonAnList = new ArrayList<>();
        String query = String.format("db.%s.find({})", CollectionNameConst.CATEGORY_COLLECTION);

        try {
            List<?> list = this.entityManager.createNativeQuery(query, DanhMucMonAn.class).getResultList();
            list.forEach(item -> danhMucMonAnList.add((DanhMucMonAn) item));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

        return danhMucMonAnList;
    }

    @Override
    public void createIndex() throws RemoteException {
        String query = String.format("db.%s.createIndex({'$**': 'text'})", CollectionNameConst.CATEGORY_COLLECTION);

        try {
            this.entityTransaction.begin();
            this.entityManager.createNativeQuery(query);
            this.entityTransaction.commit();
        } catch (Exception e) {
            this.entityTransaction.rollback();
        }
    }

    @Override
    public List<DanhMucMonAn> fullTextSearch(String keyword) throws RemoteException {
	    createIndex();
        List<DanhMucMonAn> danhMucMonAns = new ArrayList<>();
        String query = String.format("db.%s.find({$text: {$search: '%s'}})",
                CollectionNameConst.CATEGORY_COLLECTION, keyword);
        try {
            List<?> list = this.entityManager.createNativeQuery(query, DanhMucMonAn.class).getResultList();
            list.forEach(item -> danhMucMonAns.add((DanhMucMonAn) item));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return danhMucMonAns;
    }

    @Override
    public String generateId() throws RemoteException {
        int lastId = 0;
        String query = String.format("db.%s.find({}).limit(1).sort({'_id': -1})", CollectionNameConst.CATEGORY_COLLECTION);

        try {
            List<?> list = this.entityManager.createNativeQuery(query, DanhMucMonAn.class).getResultList();
            DanhMucMonAn danhMucMonAnLast = (DanhMucMonAn) list.get(list.size() - 1);

            lastId = Integer.parseInt(danhMucMonAnLast.getMaDanhMuc()
                    .substring(PatternIdConst.PREFIX_DANH_MUC.length(), danhMucMonAnLast.getMaDanhMuc().length()));
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }

        return String.format("%s%04d", PatternIdConst.PREFIX_DANH_MUC, ++lastId);
    }
}
