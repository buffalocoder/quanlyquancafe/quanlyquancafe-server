package org.buffalocoder.quanlyquancafe.server.daos;

import org.buffalocoder.quanlyquancafe.common.interfaces.IDataAccessObject;
import org.buffalocoder.quanlyquancafe.server.helpers.JpaPersistenceHelper;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

public class DataAccessObjectImpl<T> extends UnicastRemoteObject implements IDataAccessObject<T> {
	public EntityManager entityManager;
    public EntityTransaction entityTransaction;
    public Class<T> genericType;

    public DataAccessObjectImpl(EntityManager entityManager, Class<T> genericType) throws RemoteException {
        this.entityManager = entityManager;
        this.entityTransaction = entityManager.getTransaction();
        this.genericType = genericType;
    }

    public DataAccessObjectImpl(Class<T> genericType) throws RemoteException {
        this.entityManager = JpaPersistenceHelper.getInstance().getEntityManager();
        this.entityTransaction = JpaPersistenceHelper.getInstance().getEntityTransaction();
        this.genericType = genericType;
    }

    @Override
    public boolean add(T t) {
        try {
            this.entityTransaction.begin();
            this.entityManager.persist(t);
            this.entityTransaction.commit();
        } catch (Exception e) {
            this.entityTransaction.rollback();
            return false;
        }

        return true;
    }



    @Override
    public boolean update(T t) {
        try {
            this.entityTransaction.begin();
            this.entityManager.merge(t);
            this.entityTransaction.commit();
        } catch (Exception e) {
            this.entityTransaction.rollback();
            return false;
        }

        return true;
    }

    @Override
    public boolean deleteById(Object id) throws RemoteException {
        try {
            T t = this.entityManager.find(this.genericType, id);

            if (t != null) {
                this.entityTransaction.begin();
                this.entityManager.remove(t);
                this.entityTransaction.commit();
            }
        } catch (Exception e) {
            this.entityTransaction.rollback();
            return false;
        }

        return true;
    }

    @Override
    public boolean delete(T t) {
        try {
            if (t != null) {
                this.entityTransaction.begin();
                this.entityManager.remove(t);
                this.entityTransaction.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.entityTransaction.rollback();
            return false;
        }

        return true;
    }

    @Override
    public T find(Object key) {
        T t = null;

        try {
            t = this.entityManager.find(this.genericType, key);
        } catch (Exception e) {
            this.entityTransaction.rollback();
            return null;
        }

        return t;
    }

    @Override
    public List<T> find() {
        return null;
    }

    @Override
    public String generateId() throws RemoteException {
        return null;
    }

    @Override
    public void createIndex() throws RemoteException {
    }

    @Override
    public List<T> fullTextSearch(String s) throws RemoteException {
        return null;
    }
}
