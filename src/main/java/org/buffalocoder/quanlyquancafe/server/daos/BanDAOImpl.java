package org.buffalocoder.quanlyquancafe.server.daos;


import org.buffalocoder.quanlyquancafe.common.consts.CollectionNameConst;
import org.buffalocoder.quanlyquancafe.common.consts.PatternIdConst;
import org.buffalocoder.quanlyquancafe.common.entities.Ban;
import org.buffalocoder.quanlyquancafe.common.interfaces.IBanDAO;
import javax.persistence.EntityManager;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public class BanDAOImpl extends DataAccessObjectImpl<Ban> implements IBanDAO {

	public BanDAOImpl(EntityManager entityManager) throws RemoteException {
        super(entityManager, Ban.class);
        createIndex();
    }

    public BanDAOImpl() throws RemoteException {
        super(Ban.class);
        createIndex();
    }

    @Override
    public List<Ban> find() {
        List<Ban> banList = new ArrayList<>();
        String query = String.format("db.%s.find({})", CollectionNameConst.TABLE_COLLECTION);
        try {
            List<?> list = this.entityManager.createNativeQuery(query, Ban.class).getResultList();
            list.forEach(item -> banList.add((Ban) item));
        } catch (Exception e) {
            return null;
        }

        return banList;
    }

    @Override
    public void createIndex() throws RemoteException {
	    String query = String.format("db.%s.createIndex({'$**': 'text'})", CollectionNameConst.TABLE_COLLECTION);

        try {
            this.entityTransaction.begin();
            this.entityManager.createNativeQuery(query);
            this.entityTransaction.commit();
        } catch (Exception e) {
            this.entityTransaction.rollback();
        }
    }

    @Override
    public String generateId() throws RemoteException {
        int lastId = 0;
        String query = String.format("db.%s.find({}).limit(1).sort({'_id': -1})", CollectionNameConst.TABLE_COLLECTION);

        try {
            List<?> list = this.entityManager.createNativeQuery(query, Ban.class).getResultList();
            Ban banLast = (Ban) list.get(list.size() - 1);

            lastId = Integer.parseInt(banLast.getMaBan()
                    .substring(PatternIdConst.PREFIX_BAN.length(), banLast.getMaBan().length()));
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }

        return String.format("%s%04d", PatternIdConst.PREFIX_BAN, ++lastId);
    }

    @Override
    public List<Ban> fullTextSearch(String keyword) throws RemoteException {
        createIndex();
        List<Ban> banList = new ArrayList<>();
        String query = String.format("db.%s.find({$text: {$search: '%s'}})",
                CollectionNameConst.TABLE_COLLECTION, keyword);
        try {
            List<?> list = this.entityManager.createNativeQuery(query, Ban.class).getResultList();
            list.forEach(item -> banList.add((Ban) item));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return banList;
    }
}
