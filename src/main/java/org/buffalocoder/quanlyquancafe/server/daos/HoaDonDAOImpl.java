package org.buffalocoder.quanlyquancafe.server.daos;

import org.buffalocoder.quanlyquancafe.common.consts.CollectionNameConst;
import org.buffalocoder.quanlyquancafe.common.consts.PatternIdConst;
import org.buffalocoder.quanlyquancafe.common.entities.HoaDon;
import org.buffalocoder.quanlyquancafe.common.entities.MonAn;
import org.buffalocoder.quanlyquancafe.common.interfaces.IHoaDonDAO;

import javax.persistence.EntityManager;
import java.rmi.RemoteException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;

public class HoaDonDAOImpl extends DataAccessObjectImpl<HoaDon> implements IHoaDonDAO{
	public HoaDonDAOImpl(EntityManager entityManager) throws RemoteException {
        super(entityManager, HoaDon.class);
        createIndex();
    }

    public HoaDonDAOImpl() throws RemoteException {
        super(HoaDon.class);
        createIndex();
    }

    @Override
    public List<HoaDon> find() {
        List<HoaDon> hoaDonList = new ArrayList<>();
        String query = String.format("db.%s.find({})", CollectionNameConst.BILL_COLLECTION);
        try {
            List<?> list = this.entityManager.createNativeQuery(query, HoaDon.class).getResultList();
            list.forEach(item -> hoaDonList.add((HoaDon) item));
        } catch (Exception e) {
            return null;
        }

        return hoaDonList;
    }

    @Override
    public void createIndex() throws RemoteException {
        String query = String.format("db.%s.createIndex({'$**': 'text'})", CollectionNameConst.BILL_COLLECTION);

        try {
            this.entityTransaction.begin();
            this.entityManager.createNativeQuery(query);
            this.entityTransaction.commit();
        } catch (Exception e) {
            this.entityTransaction.rollback();
        }
    }

    @Override
    public List<HoaDon> fullTextSearch(String keyword) throws RemoteException {
        createIndex();
        List<HoaDon> hoaDons = new ArrayList<>();
        String query = String.format("db.%s.find({$text: {$search: '%s'}})",
                CollectionNameConst.BILL_COLLECTION, keyword);
        try {
            List<?> list = this.entityManager.createNativeQuery(query, HoaDon.class).getResultList();
            list.forEach(item -> hoaDons.add((HoaDon) item));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return hoaDons;
    }

    @Override
    public String generateId() throws RemoteException {
        int lastId = 0;
        String query = String.format("db.%s.find({}).limit(1).sort({'_id': -1})", CollectionNameConst.BILL_COLLECTION);

        try {
            List<?> list = this.entityManager.createNativeQuery(query, HoaDon.class).getResultList();
            HoaDon hoaDonLast = (HoaDon) list.get(list.size() - 1);

            lastId = Integer.parseInt(hoaDonLast.getMaHoaDon()
                    .substring(PatternIdConst.PREFIX_HOA_DON.length(), hoaDonLast.getMaHoaDon().length()));
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
        return String.format("%s%04d", PatternIdConst.PREFIX_HOA_DON, ++lastId);
    }

    @Override
    public HoaDon findHoaDonChuaThanhToanByMaBan(String maBan) throws RemoteException{
        HoaDon hoaDon = null;
        String query = String.format("db.%s.find({maBan: '%s', tienKhachTra: 0})", CollectionNameConst.BILL_COLLECTION, maBan);

        try {
            hoaDon = (HoaDon) this.entityManager.createNativeQuery(query, HoaDon.class).getResultList().get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return hoaDon;
    }

    double tongTien = 0;
	double tongDanhThu = 0;
	int soHoaDon = 0;
	double tienThuVe = 0;
	double tongDoanhThuTatCa = 0;
    @Override
    public double getTongDanhThu(LocalDate ngayThongKe) throws RemoteException {
        tongDanhThu = 0;
        soHoaDon = 0;
        tienThuVe = 0;
        tongDoanhThuTatCa = 0;
        List<HoaDon> listHoaDon = find();

        listHoaDon.forEach(hoaDon -> {
            if(hoaDon.getNgayXuatHoaDon().equals(ngayThongKe)) {
                tongDanhThu += hoaDon.getTongTien();
                tienThuVe += hoaDon.getTienKhachTra();
                soHoaDon++;
            }
            tongDoanhThuTatCa+=hoaDon.getTongTien();
        });

        return tongDanhThu;
    }
    public double getTongTienThuVe() {
        return tienThuVe;
    }
    public double getTongDoanhThuTatCa() {
        return tongDoanhThuTatCa;
    }
    @Override
    public int getTongSoDonHang() throws RemoteException {
        return soHoaDon;
    }


}
