FROM java
WORKDIR /
ADD classes/artifacts/quanlyquancafe_server_jar/quanlyquancafe-server.jar quanlyquancafe-server.jar
EXPOSE 9999
CMD java -jar quanlyquancafe-server.jar
